# Gitoyen <contact@gitoyen.net>
#
# vim: set ts=4:sw=4

# Filter limiting accepted prefixes on members and customers BGP sessions
#
# NB: the '+' sign is necessary for backhole management
#
# TODO: Content of this function could be filled semi-automatically using the IRR
function is_net_from_member(int AS)
prefix set as_prefixes;
{

    case AS {
        # Grenode
        51083:  as_prefixes = [
                        2001:912::/36+,         # Grenode
                        2001:912:1000::/36+,    # Rezine
                        2a00:5881:4000::/40+,   # Illyse Lyon
                        2a00:5881:c000::/40+    # Illyse St-Etienne
        ];
        # Tetaneutral
        197422: as_prefixes = [
                        2a01:6600:8000::/40+    # Tetaneutral
        ];
        # Lautrenet
        64600:  as_prefixes = [
                        2001:910:2000::/48+
        ];
        # FDN
        65055:  as_prefixes = [
                        2001:910:800::/40+,
                        2001:910:1000::/38+
        ];
        else: print "is_net_from_member: The AS number is unknown"; return false;
    }

    return (net ~ as_prefixes);
}

# prefix equivalent of rfc1918
function is_rfc1918() {
  return net ~ [ FC00::/7+ ];
}

# This function excludes weird networks
#  rfc1918, class D, class E
function is_martians() {
  return net ~ [ FE80::/10+, fec0::/10+, FF00::/8+, ::/96+, 0100::/64+, 2001:10::/28+, 2001:0db8::/32+, fc00::/7+ ];
}

# Routes within Gitoyen
function is_within_gitoyen() {
  return net ~ [ 2001:910::/32{33,128} ];
}

# Gitoyen's routes
function is_gitoyen() {
  return net ~ [ 2001:910::/32 ];
}

# Routes corresponding to carp prefixes routes
function is_within_carp() {
  return net ~ [ 2001:910:0:4::/64{65,128}, 2001:910:0:40::/64{65,128}, 2001:910:0:41::/64{65,128}, 2001:910:0:117::/64{65,128} ];
}

# Default route
function is_default() {
  return (net ~ [ ::/0 ]);
}
