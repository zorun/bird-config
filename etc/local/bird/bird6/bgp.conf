# Gitoyen <contact@gitoyen.net>
#
# vim: set ts=4:sw=4

#
# iBGP: Backbone
#
template bgp ibgp {
	local as 20766;
	next hop self;
	import where bgp_import(20766,"core",0);
        export where bgp_export(20766,"core");
}

protocol bgp core_yankee from ibgp {
        description "Yankee - iBGP";
	neighbor 2001:910::19 as 20766;
}
protocol bgp core_xray from ibgp {
        description "X-Ray - iBGP";
	neighbor 2001:910::4 as 20766;
}
protocol bgp core_zoulou from ibgp {
        description "Zoulou - iBGP";
	neighbor 2001:910::1 as 20766;
}


#
# Member: FDN
#
template bgp tpl_fdn {
	local as 20766;
	description "Livraison FDN";
        import where bgp_import(65055,"member",50000);
        export where bgp_export(65055,"default");
}

protocol bgp member_fdn1 from tpl_fdn {
	neighbor 2001:910:0:800::213 as 65055;
}
protocol bgp member_fdn2 from tpl_fdn {
	neighbor 2001:910:0:800::214 as 65055;
}


#
# Member: Grenode
#
template bgp tpl_grenode {
	local as 20766;
	description "Livraison Grenode";
        import where bgp_import(51083,"member",40000);
        export where bgp_export(51083,"full");
}

protocol bgp member_grenode1 from tpl_grenode {
	neighbor 2001:910:0:116::222 as 51083;
}


#
# Member: L'Autre.net
#
template bgp tpl_lautrenet {
	local as 20766;
	description "Livraison L'Autre.net";
        import where bgp_import(64600,"member",50000);
        export where bgp_export(64600,"full");
}

protocol bgp member_lautrenet1 from tpl_lautrenet {
	neighbor 2001:910:0:107::205 as 64600;
}
protocol bgp member_lautrenet2 from tpl_lautrenet {
	neighbor 2001:910:0:107::206 as 64600;
}


#
# Member: Tetaneutral
#

template bgp tpl_tetaneutral {
	local as 20766;
	description "Livraison Tetaneutral.net";
        import where bgp_import(197422,"member",40000);
        export where bgp_export(197422,"full");
}

protocol bgp member_tetaneutral from tpl_tetaneutral {
	neighbor 2001:910:0:3011::245 as 197422;
}


#
# Transit: Absolight
#
template bgp tpl_absolight {
	local as 20766;
	description "Transit Absolight";
        import where bgp_import(29608,"transit",10004);
        export where bgp_export(29608,"members");
}

protocol bgp transit_absolight from tpl_absolight {
	neighbor 2a01:678:1000:1::1 as 29608;
	description "Transit Absolight";
}


#
# Peering: FranceIX
#

template bgp tpl_franceix_rs {
	local as 20766;
        import where bgp_import(51706,"peering",26000);
        export where bgp_export(51706,"members");
	import limit 100000;
}

template bgp tpl_franceix_peers {
	local as 20766;
        import where bgp_import(51706,"peering",26000);
        export where bgp_export(51706,"members");
	import limit 10000;
}

protocol bgp franceix_rs1 from tpl_franceix_rs {
	neighbor 2001:7f8:54::250 as 51706;
       description "FranceIX Route Server 1";
}
protocol bgp franceix_rs2 from tpl_franceix_rs {
	neighbor 2001:7f8:54::251 as 51706;
       description "FranceIX Route Server 2";
}

protocol bgp franceix_6939 from tpl_franceix_peers {
	neighbor 2001:7f8:54::10 as 6939;
	description "FranceIX / AS-HURRICANE";
	import limit 50000;
}
protocol bgp franceix_20562 from tpl_franceix_peers {
	neighbor 2001:7f8:54::68 as 20562;
	description "FranceIX / AS-OPENPEERING-EU";
}
protocol bgp franceix_34019 from tpl_franceix_peers {
	neighbor 2001:7f8:54::71 as 34019;
	description "FranceIX / AS-HIVANE";
}
protocol bgp franceix_42473 from tpl_franceix_peers {
	neighbor 2001:7f8:54::85 as 42473;
	description "FranceIX / AS-ANEXIA";
}
protocol bgp franceix_197692 from tpl_franceix_peers {
	neighbor 2001:7f8:54::133 as 197692;
	description "FranceIX / AS-CONOSTIX";
}
protocol bgp franceix_29467 from tpl_franceix_peers {
	neighbor 2001:7f8:54::227 as 29467;
	description "FranceIX / AS-LUXNETWORK";
}
protocol bgp franceix_197422 from tpl_franceix_peers {
	neighbor 2001:7f8:54::233 as 197422;
	description "FranceIX / Tetaneutral";
}
protocol bgp franceix_13335 from tpl_franceix_peers {
	neighbor 2001:7f8:54::1:49 as 13335;
	description "FranceIX / Cloudfare";
}

#
# Peering: EquinIX
#
# EquinIX: route collector (only for monitoring)
template bgp tpl_equinix_rc {
        local as 20766;
        import where bgp_import(65517,"peering",22000);
        export where bgp_export(65517,"members");
        import limit 10;
}

protocol bgp equinix_rc1 from tpl_equinix_rc {
        neighbor 2001:7f8:43::6:5517:1 as 65517;
        description "EquinIX Route Collector 1";
}

# EquinIX: Route server
template bgp tpl_equinix_rs {
        local as 20766;
        import where bgp_import(24115,"peering",22000);
        export where bgp_export(24115,"members");
	import limit 100000;
}
template bgp tpl_equinix_peers {
        local as 20766;
        import where bgp_import(24115,"peering",22000);
        export where bgp_export(24115,"members");
	import limit 10000;
}

protocol bgp equinix_rs1 from tpl_equinix_rs {
        neighbor 2001:7f8:43:0:ffff:ffff:ffff:1 as 24115;
        description "EquinIX Route Server 1";
}
protocol bgp equinix_rs2 from tpl_equinix_rs {
        neighbor 2001:7f8:43:0:ffff:ffff:ffff:2 as 24115;
        description "EquinIX Route Server 2";
}
protocol bgp equinix_6939 from tpl_equinix_peers {
	neighbor 2001:7f8:43::6939:1 as 6939;
	description "EquinIX / AS-HURRICANE";
	import limit 26000;
}
protocol bgp equinix_21371_1 from tpl_equinix_peers {
	neighbor 2001:07f8:43::2:1371:1 as 21371;
	description "EquinIX / AS-EQUINIX-EU 1";
	import limit 100;
}
protocol bgp equinix_21371_2 from tpl_equinix_peers {
	neighbor 2001:07f8:43::2:1371:2 as 21371;
	description "EquinIX / AS-EQUINIX-EU 2";
	import limit 100;
}
protocol bgp equinix_198507 from tpl_equinix_peers {
	neighbor 2001:7f8:43::19:8507:1 as 198507;
	description "EquinIX / AS-QUANTIC";
	import limit 50;
}
